package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="transaction")
public class Transaction {
	public static final Transaction NONE = new Transaction();
	public static final String STATUS_RECEIVED = "Received";
	public static final String STATUS_ON_HOLD ="On Hold";
	public static final String STATUS_PREPARING ="Preparing";
	public static final String STATUS_CANCELLED ="Cancelled";
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idtransaction")
	private int id;
	@JoinColumn(name="id_purchase")
	@Column(name="id_purchases")
	private int idPurchases;
	@Column
	private String status;
	@ManyToOne
	@JoinColumn(name="id_user")
	private User user;

	@OneToMany(mappedBy="transaction", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Purchase> purchases;

	protected Transaction(){
		
	}
	
	public Transaction(User user){
		this.user = user;
		purchases = new ArrayList<Purchase>();
		this.setPreparing();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdPurchases() {
		return idPurchases;
	}
	public void setIdPurchases(int idPurchases) {
		this.idPurchases = idPurchases;
	}
	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getStatus() {
		return Sanitize.sanitizeString(status);
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setPreparing(){
		this.status = STATUS_PREPARING;
	}
	
	public void setHold(){
		this.status = STATUS_ON_HOLD;
	}
	
	public void setReceived(){
		this.status = STATUS_RECEIVED;;
	}
	
	public void setCancelled(){
		this.status = STATUS_CANCELLED;
	}

	public List<Purchase> getPurchases() {
		return purchases;
	}
	
	public void addPurchase(Purchase purchase){
		purchase.setTransaction(this);
		purchases.add(purchase);
	}

	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}

	public boolean bought(Item i) {
		for (Purchase purchase : purchases){
			if (purchase.getItem().equals(i)){
				return true;
			}
		}
		return false;
	}
	
	
	
}

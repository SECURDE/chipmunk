package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="item")
public class Item {
	public static final Item NONE = new Item();
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_item")
	private int id;
	@ManyToOne
	@JoinColumn(name="id_category")
	private Category category;
	@Column
	private String name;
	@Column
	private float price;
	@Column
	private String description;
	@Column
	private String status;

	@OneToMany(mappedBy="idItem", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Review> reviews;
	
	@Column
	private int quantity;
	protected Item(){
		
	}
	public int getId() {
		return id;
	}
	public Category getCategory() {
		return category;
	}
	public String getName() {
		return Sanitize.sanitizeString(name);
	}
	public float getPrice() {
		return price;
	}
	public String getDescription() {
		return Sanitize.sanitizeString(description);
	}
	public void discontinue() {
		this.status = "DISCONTINUED";
	}
	public String getStatus() {
		return Sanitize.sanitizeString(status);
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public void setName(String name) {
		this.name = Sanitize.sanitizeString(name);
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public void setDescription(String description) {
		this.description = Sanitize.sanitizeString(description);
	}
	
	public Item(Category category, String name, float price, String description) {
		this.category = category;
		this.name = name;
		this.price = price;
		this.description = description;
	}
	
	public void review(Review newReview) {
		this.reviews.add(newReview);
	}
	public static Item getNone() {
		return NONE;
	}
	public List<Review> getReviews() {
		return reviews;
	}
	

	public int getQuantity() {
		return this.quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Item decrementQuantity() {
		this.quantity = quantity-1;
		System.out.println("Quantity decremented");
		return this;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}

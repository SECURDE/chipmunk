package model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="users")
public class User {
	public User(String firstName,String middleInitial, String lastName, String password, String email,  String billingAddress, String shippingAddress, UserType userType){
		this.firstName = firstName;
		this.middleInitial = middleInitial;
		this.lastName = lastName;
		this.password = password;
		this.email	= email;
		this.billingAddress = billingAddress;
		this.shippingAddress = shippingAddress;
		this.userType = userType;
		this.status = STATUS_OPEN;
	}
	public User(){
		
	}
	public static final User NONE = new User();
	private static final String STATUS_LOCKED = "Locked";
	private static final String STATUS_OPEN = "Open";
	@Id 
	@Column(name="id_user")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name="id_password")
	private String password;
	@ManyToOne
	@JoinColumn(name="id_user_type")
	private UserType userType;
	@Column(name="first_name")
	private String firstName;
	@Column(name="middle_initial")
	private String middleInitial;
	@Column(name="last_name")
	private String lastName;
	@Column(name="email", unique=true)
	private String email;
	@Column(name="shipping_address")
	private String shippingAddress;
	@Column(name="billing_address")
	private String billingAddress;
	@Column(name="status")
	private String status;
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Transaction> transactions;
	@Column(name="failedLogins")
	private int failedLogins;
	public String getFirstName() {
		return Sanitize.sanitizeString(firstName);
	}


	public int getId() {
		return id;
	}


	public String getPassword() {
		return password;
	}

	public String getMiddleInitial() {
		return Sanitize.sanitizeString(middleInitial);
	}


	public String getLastName() {
		return Sanitize.sanitizeString(lastName);
	}


	public String getEmail() {
		return email;
	}


	public String getShippingAddress() {
		return shippingAddress;
	}


	public String getBillingAddress() {
		return billingAddress;
	}


	public String getStatus() {
		return Sanitize.sanitizeString(status);
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setUserType(UserType userType) {
		this.userType = userType;
	}
	public void setFirstName(String firstName) {
		this.firstName = Sanitize.sanitizeString(firstName);
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = Sanitize.sanitizeString(middleInitial);
	}
	public void setLastName(String lastName) {
		this.lastName = Sanitize.sanitizeString(lastName);
	}
	public void setEmail(String email) {
		this.email = Sanitize.sanitizeString(email);
	}
	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = Sanitize.sanitizeString(shippingAddress);
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = Sanitize.sanitizeString(billingAddress);
	}
	public void setStatus(String status) {
		this.status = Sanitize.sanitizeString(status);
	}
	
	public int getFailedLogins() {
		return failedLogins;
	}
	
	public void resetFailedLogins() {
		this.failedLogins = 0;
		open();
	}
	
	public void incrementFailedLogin() {
		this.failedLogins++;
		if (failedLogins >= 5){
			lock();
		}
	}
	public void open(){
		this.status = STATUS_OPEN;
	}
	public void lock(){
		this.status = STATUS_LOCKED;
	}
	public boolean isFinancialOfficer() {
		return this.userType.equals(UserType.ACCOUNTING_MANAGER);
	}
	public boolean isProductManager() {
		return this.userType.equals(UserType.PRODUCT_MANAGER);
	}
	public List<Transaction> getTransactions(){
		return transactions;
	}
	public boolean hasPurchased(Item i) {
		for (Transaction transaction : transactions){
			if (transaction.bought(i) && (transaction.getStatus().equals(Transaction.STATUS_RECEIVED)||transaction.getStatus().equals(Transaction.STATUS_PREPARING))){
				return true;
			}
		}
		return false;
	}
	public boolean isAdmin() {
		System.out.println("I AM A:" +this.userType.getName());
		System.out.println("THE ADMIN IS A :" +UserType.ADMINISTRATOR.getName());
		return this.userType.equals(UserType.ADMINISTRATOR);
	}
	
	public boolean isLockedOut(){
		return failedLogins >= 5;
	}
	public UserType getUserType() {
		return this.userType;
	}

	
}

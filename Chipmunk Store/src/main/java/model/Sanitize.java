package model;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

public class Sanitize {
	public Sanitize(){
		
	}
	
	public static String sanitizeJSoup(String input){
		System.out.println("Cleaning using JSoup...");
		String clean = Jsoup.clean(input, Whitelist.none());
		System.out.println(input);
		System.out.println(clean);
		return clean;
	}
	
	public static String sanitizeString(String input){
		if(input!= null){
			System.out.println("Cleaning Manually...");
			String clean = input.replaceAll("\\<.*?>", "");
			System.out.println(input);
			System.out.println(clean);
			return clean;
			}
		else
			return "";
	}
}

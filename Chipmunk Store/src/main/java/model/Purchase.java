package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import repository.ItemRepository;

@Entity
@Table (name="purchase")
public class Purchase {
	public static final Purchase NONE = new Purchase();
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_purchase")
	private int id;
	@Column(name="quantity")
	private int quantity;
	@Column(name="status")
	private String status;
	@ManyToOne
	@JoinColumn(name="id_item")
	private Item item;
	@ManyToOne
	@JoinColumn(name="id_transaction")
	private Transaction transaction;
	
	protected Purchase(){
		
	}
	
	public Purchase(Item item, int quantity){
		this.quantity = quantity;
		this.item = item;
	}
	
	public void cancelled(){
		this.status = "CANCELLED";
	}

	public void purchased(){
		this.status = "PURCHASED";
	}
	
	public void exceed(){
		this.status = "LIMIT EXCEEDED";
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return Sanitize.sanitizeString(status);
	}

	public void setStatus(String status) {
		this.status = Sanitize.sanitizeString(status);
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}

	
}

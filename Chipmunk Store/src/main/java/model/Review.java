package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="review")

public class Review {
	public static final Review NONE = new Review();
	
	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_review")
	private int id;
	@ManyToOne
	@JoinColumn(name="id_user", nullable=false)
	private User idUser;
	@ManyToOne
	@JoinColumn(name="id_item", nullable=false)
	private Item idItem;
	private String content;
	private int rating;
	
	protected Review() {}
	
	public Review(User idUser, Item idItem, String content, int rating) {
		super();
		this.idUser = idUser;
		this.idItem = idItem;
		this.content = Sanitize.sanitizeString(content);
		this.rating = rating;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public void setIdUser(User idUser) {
		this.idUser = idUser;
	}
	public Item getIdItem() {
		return idItem;
	}
	public void setIdItem(Item idItem) {
		this.idItem = idItem;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = Sanitize.sanitizeString(content);
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public String getUser(){
		return this.idUser.getFirstName();
	}
}

package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_type")
public class UserType {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id_user_type")
	private int id;
	@Column(name="type_name")
	private String name;
	@Column(name="can_restock")
	private boolean canRestock;
	@Column(name="can_buy")
	private boolean canBuy;
	@Column(name="can_unlock_user")
	private boolean canUnlockUser;
	@Column(name="can_edit_product")
	private boolean canEditProduct;
	
	public static final UserType CUSTOMER = new UserType(1, "CUSTOMER", false, true, false, false);
	public static final UserType ACCOUNTING_MANAGER = new UserType(2, "ACCOUNTING MANAGER", false, false, false, false);
	public static final UserType PRODUCT_MANAGER = new UserType(3, "PRODUCT MANAGER", true, false, false, true);
	public static final UserType ADMINISTRATOR = new UserType(4, "ADMINISTRATOR", false, false, true, false);
	protected UserType(){};
	private UserType(int id, String name, boolean canRestock, boolean canBuy, boolean canUnlockUser,
			boolean canEditProduct) {
		this.id = id;
		this.name = name;
		this.canRestock = canRestock;
		this.canBuy = canBuy;
		this.canUnlockUser = canUnlockUser;
		this.canEditProduct = canEditProduct;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return Sanitize.sanitizeString(name);
	}
	public void setName(String name) {
		this.name = Sanitize.sanitizeString(name);
	}
	public boolean isCanRestock() {
		return canRestock;
	}
	public void setCanRestock(boolean canRestock) {
		this.canRestock = canRestock;
	}
	public boolean isCanBuy() {
		return canBuy;
	}
	public void setCanBuy(boolean canBuy) {
		this.canBuy = canBuy;
	}
	public boolean isCanUnlockUser() {
		return canUnlockUser;
	}
	public void setCanUnlockUser(boolean canUnlockUser) {
		this.canUnlockUser = canUnlockUser;
	}
	public boolean isCanEditProduct() {
		return canEditProduct;
	}
	public void setCanEditProduct(boolean canEditProduct) {
		this.canEditProduct = canEditProduct;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserType other = (UserType) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}

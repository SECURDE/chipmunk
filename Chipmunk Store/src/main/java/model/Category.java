package model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="category")
public class Category {
	public static final Category NONE = new Category();
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="idcategory")
	private int id;
	@Column(name="name")
	private String name;
	@OneToMany(mappedBy="category", fetch=FetchType.EAGER)
	private List<Item> items;
	
	protected Category(){}

	public String getName(){
		return Sanitize.sanitizeString(name);
	}
	public List<Item> getItems() {
		return items;
	}
}


package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import model.Category;
import model.Item;

public class ItemRepository extends Repository<Item>{
	public Item selectRow(int id){
		openEntityManager();
		Item i = entityManager.find(Item.class, id);
		closeEntityManager();
		if (i == null){
			return Item.NONE;
		}else{
			return i;
		}
	}

	public void insert(Item item) {
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			entityManager.persist(item);
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			closeEntityManager();
		}
	}

	public void update(Item item) {
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			entityManager.merge(item);
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			closeEntityManager();
		}
	}

	public List<Item> selectAll() {
		openEntityManager();
		List<Item> i = new ArrayList<Item>();
		try{
			TypedQuery<Item> query = entityManager.createQuery("SELECT a FROM Item a", Item.class);
			
			i = query.getResultList();
			
		}finally{
			closeEntityManager();
		}
		return i;
	}
	
	public List<Item> selectByName(String name) {
		openEntityManager();
		List<Item> i = new ArrayList<Item>();
		try{
			TypedQuery<Item> query = entityManager.createQuery("SELECT a FROM Item a WHERE name LIKE name", Item.class);
			query.setParameter("name", name);
			
			i = query.getResultList();
			
		}finally{
			closeEntityManager();
		}
		return i;
	}

}
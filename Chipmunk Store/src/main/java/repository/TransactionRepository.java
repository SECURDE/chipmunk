package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;

import model.Purchase;
import model.Transaction;

public class TransactionRepository extends Repository<Transaction>{
	
	public Transaction selectRow(int id){
		openEntityManager();
		Transaction t = entityManager.find(Transaction.class, id);
		closeEntityManager();
		
		if(t == null){
			return Transaction.NONE;
		} else {
			return t;
		}
	}
	
	public List<Transaction> selectRows(int idUser){
		openEntityManager();
		List<Transaction> t = new ArrayList<Transaction>();
		
		try{
			TypedQuery<Transaction> query = entityManager.createQuery("SELECT a FROM Transaction a WHERE id_user=:id_user", Transaction.class);
			query.setParameter("id_user", idUser);
			
			t = query.getResultList();
		}
		finally{
			closeEntityManager();
		}
		
		return t;
	}
	
	public void insert(Transaction transaction){
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			Session session = entityManager.unwrap(Session.class);
			
			for (Purchase purchases : transaction.getPurchases()){
				if (entityManager.contains(purchases.getItem())){
					entityManager.remove(purchases.getItem());
				}
				
				if (session.contains(purchases.getItem())){
					session.evict(purchases.getItem());
				}
				
			}
			session.clear();
			entityManager.clear();
			
			entityManager.persist(transaction);
			System.out.println("TRANSACTION PERSISTED: " + transaction.getId());
			for (Purchase purchases : transaction.getPurchases()){
				System.out.println("PURCHASE: " + purchases.getId());
				System.out.println("QUANTITY: " + purchases.getQuantity());
				
			}
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			
			closeEntityManager();
		}
	}
	
	public void update(Transaction transaction){
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			entityManager.merge(transaction);
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			closeEntityManager();
		}
	}
	
}

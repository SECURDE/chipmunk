package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import model.Purchase;

public class PurchaseRepository extends Repository<Purchase> {
	
	public Purchase selectRow(int id){
		openEntityManager();
		Purchase p = entityManager.find(Purchase.class, id);
		closeEntityManager();
		
		if(p == null){
			return Purchase.NONE;
		} else {
			return p;
		}
	}
	
	public List<Purchase> selectAll(){
		openEntityManager();
		List<Purchase> p = new ArrayList<Purchase>();
		try{
			TypedQuery <Purchase> query = entityManager.createQuery("SELECT p FROM Purchase p", Purchase.class);
			p = query.getResultList();
		}
		finally{
			closeEntityManager();
		}
		return p;
	}
	
	public List<Purchase> selectAll(int itemId){
		openEntityManager();
		List<Purchase> p = new ArrayList<Purchase>();
		try{
			TypedQuery <Purchase> query = entityManager.createQuery("SELECT p FROM Purchase p WHERE id_item =:id_item", Purchase.class);
			query.setParameter("id_item", itemId);
			p = query.getResultList();
		}
		finally{
			closeEntityManager();
		}
		return p;
	}
	
	public List<Purchase> selectRows(int idTransaction){
		openEntityManager();
		List<Purchase> p = new ArrayList<Purchase>();
		
		try{
			TypedQuery<Purchase> query = entityManager.createQuery("SELECT a FROM Purchase a WHERE id_transaction=:id_transaction", Purchase.class);
			query.setParameter("id_transaction", idTransaction);
			
			p = query.getResultList();
		}
		finally{
			closeEntityManager();
		}
		
		return p;
	}
	
	public void insert(Purchase purchase){
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			entityManager.persist(purchase);
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			closeEntityManager();
		}
	}
	
	public void update(Purchase purchase){
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			entityManager.merge(purchase);
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			closeEntityManager();
		}
	}
	
	public Purchase getPurchaseByUserAndItem (int idUser, int idItem) {
		openEntityManager();
		List<Purchase> p = new ArrayList<Purchase>();
		
		try{
			TypedQuery<Purchase> query = entityManager.createQuery("SELECT a FROM Purchase a WHERE id_user=:id_user AND id_item:= id_item", Purchase.class);
			query.setParameter("id_user", idUser);
			query.setParameter("id_item", idItem);
			p = query.getResultList();
		}
		finally{
			closeEntityManager();
		}
		
		if (p.size() > 0)
			return p.get(p.size() - 1);
		else
			return null;
	}
	
}

package repository;

import javax.persistence.EntityManager;

public abstract class Repository<T> {

	protected EntityManager entityManager = PersistenceManager.INSTANCE.getEntityManager();
	
	protected void openEntityManager(){
		entityManager = PersistenceManager.INSTANCE.getEntityManager();
		entityManager.clear();
	}
	
	protected void closeEntityManager(){
		if (entityManager.isOpen()){
			entityManager.clear();
			entityManager.close();
		}
		
	}
	
	protected String getStringValue(Object obj, String nullVal){
		if (obj == null){
			if (nullVal == null){
				return "";
			}
			return nullVal;
		}
		return obj.toString();
	}
	
	protected String getStringValue(Object obj){
		if (obj == null || obj == "" || obj == " "){
			return "";
		}
		return obj.toString();
	}
	
}

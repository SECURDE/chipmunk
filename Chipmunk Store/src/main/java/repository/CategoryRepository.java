package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import model.Category;

public class CategoryRepository extends Repository<Category>{
	public Category selectRow(String name){ ///////
		openEntityManager();
		try{
			TypedQuery<Category> query = entityManager.createQuery("SELECT a FROM Category a WHERE name=:name", Category.class);
			query.setParameter("name", name);
			if(query.getResultList().isEmpty()){
				System.out.println("category not found");
				return Category.NONE;
			}
			else{
				return query.getSingleResult();
			}
		}finally{
			closeEntityManager();
		}	
	}

	public List<Category> selectAll() {
		openEntityManager();
		List<Category> c = new ArrayList<Category>();
		try{
			TypedQuery<Category> query = entityManager.createQuery("SELECT a FROM Category a", Category.class);
			
			c = query.getResultList();
			
		}finally{
			closeEntityManager();
		}
		return c;
	}
}

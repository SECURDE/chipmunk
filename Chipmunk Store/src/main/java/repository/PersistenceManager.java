package repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public enum PersistenceManager{
  INSTANCE;

  private EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("chipmunk");

  public EntityManager getEntityManager() {
    return emFactory.createEntityManager();
  }

  public void close() {
    emFactory.close();
  }
  
}
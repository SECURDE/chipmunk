package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;

import model.Category;
import model.Item;
import model.User;

public class UserRepository extends Repository<User>{

	public User selectRow(int id){
		openEntityManager();
		
		User u = entityManager.find(User.class, id);
		
		closeEntityManager();
		
		if(u == null){
			return User.NONE;
		}

		return u;
	}
	
	public User selectRow(String email){
		openEntityManager();
		User u = User.NONE;
		try{
			TypedQuery<User> query = entityManager.createQuery("SELECT a FROM User a WHERE email=:email", User.class);
			query.setParameter("email", email);
			if (!query.getResultList().isEmpty()){
				u = query.getSingleResult();
			}
			
		}finally{
			closeEntityManager();
		}
		System.out.println("I fished out a:"+u.getFirstName());
		return u;
	}
	
	public void insertRow(User user){
		
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			entityManager.persist(user);
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			closeEntityManager();
		}
	}
	
	public void update(User user) {
		openEntityManager();
		entityManager.getTransaction().begin();
		try{
			entityManager.merge(user);
			entityManager.flush();
			entityManager.getTransaction().commit();
		}
		finally{
			closeEntityManager();
		}
	}
	
	
	public List<User> selectAllLocked() {
		openEntityManager();
		List<User> u = new ArrayList<User>();
		try{
			TypedQuery<User> query = entityManager.createQuery("SELECT a FROM User a WHERE status = 'locked'", User.class);
			//WHERE status = 'locked'
			
			u = query.getResultList();
			
		}finally{
			closeEntityManager();
		}
		return u;
	}

	
}



package actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Category;
import model.Item;
import model.User;
import security.ServletSecurity;
import service.ProductService;
import service.UserService;

/**
 * Servlet implementation class DeleteProduct
 */
@WebServlet("/delete_product")
public class DeleteProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{ 
		User user =  ServletSecurity.getLoggedInUser(request);
		
		File productLog = new File ("..\\Logs\\ProductLogs.txt");
		if (!productLog.exists()) {
			productLog.createNewFile();
		}
		
		FileWriter productLogFileWriter = new FileWriter (productLog, true);
		BufferedWriter productLogBuffWriter = new BufferedWriter(productLogFileWriter);
		PrintWriter productLogPrintWriter = new PrintWriter (productLogBuffWriter);

		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		System.out.println("HAS CALLED DETELE PRODUCT");
		if (user.isProductManager()){
			int itemId = Integer.parseInt(request.getParameter("p"));
			
			System.out.println("ATTEMPTING TO SLAY:"+ itemId);
			Item i = ProductService.getItem(itemId);
			
			if (i.equals(Item.NONE)){
				productLogPrintWriter.println("[" + currentDateString + "]" + "[DELETE][FAIL]User: " + user.getEmail() + " failed to delte product");
				productLogPrintWriter.close();
				productLogBuffWriter.close();
				productLogFileWriter.close();
				response.sendRedirect("error");
			}else{
				System.out.println("Should be deleting"+ itemId);
				ProductService.deleteItem(i);
				Category cat = i.getCategory();
				productLogPrintWriter.println("[" + currentDateString + "]" + "[DELETE][SUCCESS]User: " + user.getEmail() + 
						" deleted product: id = " + itemId +
						", name = " + i.getName());
				productLogPrintWriter.close();
				productLogBuffWriter.close();
				productLogFileWriter.close();
				response.sendRedirect("productsManager?c=" + cat.getName());
			}
		}else{
			productLogPrintWriter.println("[" + currentDateString + "]" + "[DELETE][FAIL]User: " + user.getEmail() + " tried to delte product but is not product manager");
			productLogPrintWriter.close();
			productLogBuffWriter.close();
			productLogFileWriter.close();
			response.sendRedirect("error");
		}
		
	}

}

package actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Category;
import model.Item;
import model.Sanitize;
import model.User;
import security.ServletSecurity;
import service.ProductService;
import service.UserService;

/**
 * Servlet implementation class AddProduct
 */
@WebServlet("/add_product")
public class AddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/404.html").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user =  ServletSecurity.getLoggedInUser(request);
		
		File productLog = new File ("..\\Logs\\ProductLogs.txt");
		if (!productLog.exists()) {
			productLog.createNewFile();
		}
		
		FileWriter productLogFileWriter = new FileWriter (productLog, true);
		BufferedWriter productLogBuffWriter = new BufferedWriter(productLogFileWriter);
		PrintWriter productLogPrintWriter = new PrintWriter (productLogBuffWriter);

		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		if (user.isProductManager()){
			try{
				System.out.println("Attempting to add product");
				String name = Sanitize.sanitizeString(request.getParameter("name"));
				float price = Float.parseFloat(request.getParameter("price"));
				int quantity = Integer.parseInt(request.getParameter("quantity"));
				String desc = Sanitize.sanitizeString(request.getParameter("desc"));
				String cat = Sanitize.sanitizeString(request.getParameter("category"));
				
				Category c = ProductService.getCategoryByName(cat);
				
				if(!(price > Float.MAX_VALUE || price < Float.MIN_VALUE)){
					if (c.equals(Category.NONE)){
						productLogPrintWriter.println("[" + currentDateString + "]" + "[ADD][FAIL]User: " + user.getEmail() + " failed to add new product");
						productLogPrintWriter.close();
						productLogBuffWriter.close();
						productLogFileWriter.close();
						response.sendRedirect("error");
					}else{
						Item i = new Item(c, name, price, desc);
						i.setQuantity(quantity);
						ProductService.createItem(i);
						productLogPrintWriter.println("[" + currentDateString + "]" + "[ADD][SUCCESS]User: " + user.getEmail() + 
								" added new product: name = " + name +
								", price = " + price +
								", quantity = " + quantity +
								", description = " + desc +
								", category = " + cat);
						productLogPrintWriter.close();
						productLogBuffWriter.close();
						productLogFileWriter.close();
						request.setAttribute("product", i);
						request.getRequestDispatcher("/product_detail.jsp").forward(request, response);
						return;
					}
				} else {
					System.out.println("FLOAT OVERFLOW/UNDERFLOW!");
					response.sendRedirect("error");
				}
			} catch(NumberFormatException e){
				System.out.println("INTEGER OVERFLOW/UNDERFLOW!");
				response.sendRedirect("error");
			}
		}else{
			System.out.println("YOU ARE NOT PRODUCT MANAGER");
			productLogPrintWriter.println("[" + currentDateString + "]" + "[ADD][FAIL]User: " + user.getEmail() + " tried to add new product but is not product manager");
			productLogPrintWriter.close();
			productLogBuffWriter.close();
			productLogFileWriter.close();
			response.sendRedirect("/Chipmunk_Store");
			return;
			
		}
			
	}

}

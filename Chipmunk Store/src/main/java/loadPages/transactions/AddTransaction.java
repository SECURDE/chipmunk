package transactions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import model.Purchase;
import model.Transaction;
import model.User;
import security.ServletSecurity;
import service.ProductService;
import service.PurchaseService;
import service.TransactionService;
import service.UserService;

/**
 * Servlet implementation class AddTransaction
 */
@WebServlet("/addtransaction")
public class AddTransaction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddTransaction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		File transactionLog = new File ("..\\Logs\\TransactionLogs.txt");
		if (!transactionLog.exists()) {
			transactionLog.createNewFile();
		}
		
		FileWriter transactionLogFileWriter = new FileWriter (transactionLog, true);
		BufferedWriter transactionLogBuffWriter = new BufferedWriter(transactionLogFileWriter);
		PrintWriter transactionLogPrintWriter = new PrintWriter (transactionLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		if (ServletSecurity.isUserLoggedIn(request)){
			
			User u = ServletSecurity.getLoggedInUser(request);
			
			// SIMULATE CREDIT CARD CREDENTIALS //
			float total = Float.parseFloat(request.getParameter("grandtotal"));
			String name = request.getParameter("name");
			String cardNum = request.getParameter("cardNum");
			int securityNum = Integer.parseInt(request.getParameter("securityNum"));
			int month = Integer.parseInt(request.getParameter("month"));
			int year = Integer.parseInt(request.getParameter("year"));
			
			boolean isUnderLimit = false;
			if(total > 1000000){
				isUnderLimit = false;
				System.out.println("isNOTUnderLimit");
			} else {
				isUnderLimit = true;
				System.out.println("isUnderLimit");
			}
			
			boolean isCorrectName = false;
			if(!(name.equalsIgnoreCase("Kaye Solomon"))){
				isCorrectName = false;
				System.out.println("isNOTCorrectName");
			} else {
				isCorrectName = true;
				System.out.println("isCorrectName");
			}
			
			boolean isCorrectCardNum = false;
			if(!(cardNum.equalsIgnoreCase("1234567890123456"))){
				isCorrectCardNum = false;
				System.out.println("isNOTCorrectCardNum");
			} else {
				isCorrectCardNum = true;
				System.out.println("isCorrectCardNum");
			}
			
			boolean isCorrectSecurityNum = false;
			if(securityNum != 987){
				isCorrectSecurityNum = false;
				System.out.println("isNOTCorrectSecurityNum");
			} else {
				isCorrectSecurityNum = true;
				System.out.println("isCorrectSecurityNum");
			}
			
			boolean isCorrectMonth = false;
			if(month != 5){
				isCorrectMonth = false;
				System.out.println("isNOTCorrectMonth");
			} else {
				isCorrectMonth = true;
				System.out.println("isCorrectMonth");
			}
			
			boolean isCorrectYear = false;
			if(year != 2021){
				isCorrectYear = false;
				System.out.println("isNOTCorrectYear");
			} else {
				isCorrectYear = true;
				System.out.println("isCorrectYear");
			}
			
			if(isUnderLimit && isCorrectName && isCorrectCardNum && isCorrectSecurityNum && isCorrectMonth && isCorrectYear){
				// SAVE TRANSACTION //
				Transaction t = new Transaction(u);
				
				// SAVE PURCHASES //
				ArrayList<Integer> itemIds = (ArrayList<Integer>) session.getAttribute("itemids");
				ArrayList<Integer> quantities = (ArrayList<Integer>) session.getAttribute("itemquantities");
				
				// UPDATE CART FIRST //
				for(int i = 0; i < quantities.size(); i++){
					String cartQty = "qty" + i;
					try{
						if(Integer.parseInt(request.getParameter(cartQty)) > 0){
							quantities.set(i, Integer.parseInt(request.getParameter(cartQty)));
						} else {
							quantities.set(i, 1);
						}
					} catch(NumberFormatException ex) {
						System.out.println("INTEGER OVERFLOW/UNDERFLOW!");
						quantities.set(i, 1);
					}
				}
				
				for(int i = 0; i < itemIds.size(); i++){
					
					Item item = ProductService.getItem(itemIds.get(i));
					System.out.println(item.getId());
					
					if(item.getQuantity()-quantities.get(i)<0){
						System.out.println("Oh no you don't");
						transactionLogPrintWriter.println("[" + currentDateString + "]" + "[TRANSACTION][FAIL]User: " + u.getEmail() +
								" failed to purchase due to missing items.");
						transactionLogPrintWriter.close();
						transactionLogBuffWriter.close();
						transactionLogFileWriter.close();
						request.getRequestDispatcher("/index.jsp").forward(request, response);
						return;
					}else{
						item.setQuantity(item.getQuantity()-quantities.get(i));
						ProductService.updateItem(item);
						Purchase p = new Purchase(item, quantities.get(i));
						t.addPurchase(p);
					}
				}
				TransactionService.createTransaction(t);
				session.removeAttribute("cartitems");
				session.removeAttribute("itemids");
				session.removeAttribute("cartsize");
				session.removeAttribute("itemquantities");
				
				
				transactionLogPrintWriter.print("[" + currentDateString + "]" + "[TRANSACTION][SUCCESS]User: " + u.getEmail() +
						" made the following transactions:");
				
				for (Purchase purchases : t.getPurchases()) {
					transactionLogPrintWriter.print(" item id: " + purchases.getId() + 
							" - item quantity: " + purchases.getQuantity());
				}
				
				transactionLogPrintWriter.println();
				
				transactionLogPrintWriter.close();
				transactionLogBuffWriter.close();
				transactionLogFileWriter.close();
				
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			} else {
				System.out.println("Wrong credentials!");
				transactionLogPrintWriter.println("[" + currentDateString + "]" + "[TRANSACTION][FAIL]User: " + u.getEmail() +
						" failed to purchase due to wrong credentials.");
				transactionLogPrintWriter.close();
				transactionLogBuffWriter.close();
				transactionLogFileWriter.close();
				response.sendRedirect("error");
			}
		} else {
			transactionLogPrintWriter.println("[" + currentDateString + "]" + "[TRANSACTION][FAIL]Annonymous user tried to purchase items");
			transactionLogPrintWriter.close();
			transactionLogBuffWriter.close();
			transactionLogFileWriter.close();
			response.sendRedirect("/Chipmunk_Store");

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

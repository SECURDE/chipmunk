package users;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Sanitize;
import model.User;
import security.ServletSecurity;
import service.UserService;

/**
 * Servlet implementation class LoadProducts
 */
@WebServlet("/changePassword")
public class changePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public changePassword() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doPost(request, response);
		}

	/**;
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user =  ServletSecurity.getLoggedInUser(request);
		
		String oldPassword = (String) request.getParameter("oldPassword");
		
		String newPassword = (String) request.getParameter("newPassword");
		String newPassword2 = (String) request.getParameter("newPassword2");
		
		File userLog = new File ("..\\Logs\\UserLogs.txt");
		if (!userLog.exists()) {
			userLog.createNewFile();
		}
		
		FileWriter userLogFileWriter = new FileWriter (userLog, true);
		BufferedWriter userLogBuffWriter = new BufferedWriter(userLogFileWriter);
		PrintWriter userLogPrintWriter = new PrintWriter (userLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		if(UserService.verifyUser(user.getEmail(), oldPassword) && (newPassword.equals(newPassword2))){
			try {
				user.setPassword(UserService.hash(newPassword2));
				userLogPrintWriter.println("[" + currentDateString + "]" + "[CHANGE PASSWORD][SUCCESS]User: " + user.getEmail() + " has successfully changed their password.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				userLogPrintWriter.println("[" + currentDateString + "]" + "[CHANGE PASSWORD][FAIL]User: " + user.getEmail() + " failed to change their password.");
				e.printStackTrace();
			}
			userLogPrintWriter.close();
			userLogBuffWriter.close();
			userLogFileWriter.close();
			UserService.updateUser(user, oldPassword);

		}
		else{
			userLogPrintWriter.println("[" + currentDateString + "]" + "[CHANGE PASSWORD][FAIL]User: " + user.getEmail() + " failed to change their password.");
			userLogPrintWriter.close();
			userLogBuffWriter.close();
			userLogFileWriter.close();
			
		}
		request.getRequestDispatcher("/userData").forward(request, response);	
		return;
	}

}
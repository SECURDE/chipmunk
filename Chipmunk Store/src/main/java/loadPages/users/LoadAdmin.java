package users;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import security.ServletSecurity;
import service.UserService;

/**
 * Servlet implementation class LoadAccountManager
 */
@WebServlet("/LoadAdmin")
public class LoadAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserService us= new UserService();
		System.out.println("LOAD ADMIN GET");
		if(ServletSecurity.isUserLoggedIn(request) && ServletSecurity.getLoggedInUser(request).isAdmin() ){
			System.out.println("loggedin");
			request.setAttribute("lockedUsers", us.getLockedAccounts());
			request.getRequestDispatcher("/administrator.jsp").forward(request, response);	
			return;
		}
		System.out.println("notloggedin go home");
		response.sendRedirect("error");
		return;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	
	}

}

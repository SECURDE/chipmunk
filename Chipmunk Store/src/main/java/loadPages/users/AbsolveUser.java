package users;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Sanitize;
import model.User;
import security.ServletSecurity;
import service.UserService;

/**
 * Servlet implementation class AbsolveUser
 */
@WebServlet("/AbsolveUser")
public class AbsolveUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AbsolveUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("DO GET ABSOLVE");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String email = Sanitize.sanitizeString(request.getParameter("email"));
		String password = request.getParameter("password");
		
		File userLog = new File ("..\\Logs\\UserLogs.txt");
		if (!userLog.exists()) {
			userLog.createNewFile();
		}
		
		FileWriter userLogFileWriter = new FileWriter (userLog, true);
		BufferedWriter userLogBuffWriter = new BufferedWriter(userLogFileWriter);
		PrintWriter userLogPrintWriter = new PrintWriter (userLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		User loggedInUser = ServletSecurity.getLoggedInUser(request);
		
		if (ServletSecurity.isUserLoggedIn(request) && loggedInUser.isAdmin()){
			User tempUser = UserService.getUser(email);
			tempUser.resetFailedLogins();
			UserService.updateUserByAdmin(tempUser, loggedInUser);
			session.setAttribute("failedLogins", 0);
			System.out.println("Absolution Complete");
			userLogPrintWriter.println("[" + currentDateString + "]" + "[ABSOLVE][SUCCESS]User: " + loggedInUser.getEmail() +
					" successfully absolved user: " + email);
			userLogPrintWriter.close();
			userLogBuffWriter.close();
			userLogFileWriter.close();
			getServletContext().getRequestDispatcher("/LoadAdmin").forward(request, response);
		}else
		{
			System.out.println("You lack the power for that");

			System.out.println("You are: " +loggedInUser.getEmail());
			System.out.println("You are logged in: " +session.getAttribute("isLoggedIn"));
			System.out.println("And your password is: " +password);
			System.out.println("Are you verified: "+UserService.verifyUser(loggedInUser.getEmail(), password));
			System.out.println("Are you an admin: "+ loggedInUser.isAdmin());
			
			userLogPrintWriter.println("[" + currentDateString + "]" + "[ABSOLVE][FAIL]User: " + loggedInUser.getEmail() +
					"tried to absolved user: " + email +
					" but is not an admin");
			userLogPrintWriter.close();
			userLogBuffWriter.close();
			userLogFileWriter.close();
			
		}
		
	}

}

package users;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;
import security.ServletSecurity;
import service.UserService;

/**
 * Servlet implementation class LoadProducts
 */
@WebServlet("/userData")
public class LoadUserData extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadUserData() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**;
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletSecurity.isUserLoggedIn(request)){
			User currentUser = ServletSecurity.getLoggedInUser(request);
			request.setAttribute("firstName", currentUser.getFirstName());
			request.setAttribute("middleInitial", currentUser.getMiddleInitial());
			request.setAttribute("lastName", currentUser.getLastName());
			request.setAttribute("email", currentUser.getEmail());
			request.setAttribute("billingAddress", currentUser.getBillingAddress());
			request.setAttribute("shippingAddress", currentUser.getShippingAddress());
			request.getRequestDispatcher("/customer-account.jsp").forward(request, response);
		}else{
			request.getRequestDispatcher("/index.jsp").forward(request, response);	
		}
	
	}

}

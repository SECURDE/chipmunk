package users;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Sanitize;
import model.User;
import security.ServletSecurity;
import service.UserService;

/**
 * Servlet implementation class LoadProducts
 */
@WebServlet("/editUser")
public class EditUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doPost(request, response);
		}

	/**;
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String currentEmail = Sanitize.sanitizeString((String)session.getAttribute("loggedInEmail"));

		String firstName = Sanitize.sanitizeString((String) request.getParameter("firstName"));
		String middleInitial = Sanitize.sanitizeString((String) request.getParameter("middleInitial"));
		String lastName = Sanitize.sanitizeString((String) request.getParameter("lastName"));
		
		String password = (String) request.getParameter("password");
		String confirmPassword = (String) request.getParameter("confirmPassword");
		String email = Sanitize.sanitizeString((String) request.getParameter("email"));
		String billingAddress = Sanitize.sanitizeString((String) request.getParameter("billingAddress"));
		String shippingAddress = Sanitize.sanitizeString((String) request.getParameter("shippingAddress"));

		File userLog = new File ("..\\Logs\\UserLogs.txt");
		if (!userLog.exists()) {
			userLog.createNewFile();
		}
		
		FileWriter userLogFileWriter = new FileWriter (userLog, true);
		BufferedWriter userLogBuffWriter = new BufferedWriter(userLogFileWriter);
		PrintWriter userLogPrintWriter = new PrintWriter (userLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		System.out.println("Test if ok currentEmail:" + UserService.decrypt(currentEmail) +" password: "+ confirmPassword);
		if (UserService.verifyUser(UserService.decrypt(currentEmail), confirmPassword)){
			System.out.println("You have been granted access to change credentials");
			userLogPrintWriter.println("[" + currentDateString + "]" + "[EDIT INFORMATION][SUCCESS]User: " + email + 
					"changed their details to: first name = " + firstName +
					", middle initial = " + middleInitial +
					", last name = " + lastName +
					", email = " + email +
					", billing address = " + billingAddress +
					", shipping address = " + shippingAddress);
			
			System.out.println("IT OK");
			User user = ServletSecurity.getLoggedInUser(request);
			user.setFirstName(firstName);
			user.setMiddleInitial(middleInitial);
			user.setLastName(lastName);
			user.setEmail(email);
			user.setBillingAddress(billingAddress);
			user.setShippingAddress(shippingAddress);

			UserService.updateUser(user, password);

		}else{
			System.out.println("CREDENTIALS WRONG, can't change user details");
			userLogPrintWriter.println("[" + currentDateString + "]" + "[EDIT INFORMATION][FAIL]User: " + email + " failed to edit their details.");
		}

		userLogPrintWriter.close();
		userLogBuffWriter.close();
		userLogFileWriter.close();
		request.getRequestDispatcher("/userData").forward(request, response);	
		return;
		
	}
}

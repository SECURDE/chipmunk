package users;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Sanitize;
import model.User;
import model.UserType;
import service.UserService;

/**
 * Servlet implementation class LoadProducts
 */
@WebServlet("/register")
public class registerUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public registerUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("isLoggedIn", false);
		
		String firstName= Sanitize.sanitizeString(request.getParameter("firstName"));
		String middleInitial= Sanitize.sanitizeString(request.getParameter("middleInitial"));
		String lastName= Sanitize.sanitizeString(request.getParameter("lastName"));
		String password = request.getParameter("password");
		String email= Sanitize.sanitizeString(request.getParameter("email"));
		String shippingAddress = Sanitize.sanitizeString(request.getParameter("shippingAddress"));
		String billingAddress= Sanitize.sanitizeString(request.getParameter("billingAddress"));
		
		File userLog = new File ("..\\Logs\\UserLogs.txt");
		if (!userLog.exists()) {
			userLog.createNewFile();
		}
		
		FileWriter userLogFileWriter = new FileWriter (userLog, true);
		BufferedWriter userLogBuffWriter = new BufferedWriter(userLogFileWriter);
		PrintWriter userLogPrintWriter = new PrintWriter (userLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		String hashword = "";
		try {
			hashword = UserService.hash(password);
			User trial = UserService.getUser(email);
			if (trial == null || trial.equals(User.NONE)){
				//Error: User already exists
				userLogPrintWriter.println("[" + currentDateString + "]" + "[REGISTER][FAIL]User: " + email +
						"tried to register again");
				userLogPrintWriter.close();
				userLogBuffWriter.close();
				userLogFileWriter.close();
				response.sendRedirect("error");
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendRedirect("error");
		}
		User newUser = new User(firstName,middleInitial, lastName, hashword, email, billingAddress, shippingAddress, UserType.CUSTOMER);

		UserService.createUser(newUser);
		
		userLogPrintWriter.println("[" + currentDateString + "]" + "[REGISTER][SUCCESS]New user registration: " + email);
		userLogPrintWriter.close();
		userLogBuffWriter.close();
		userLogFileWriter.close();
		
		response.sendRedirect("/Chipmunk_Store");
	}

}

package users;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Sanitize;
import model.User;
import service.UserService;

/**
 * Servlet implementation class LoadProducts
 */
@WebServlet("/users")
public class LoadUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadUsers() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("isLoggedIn", false);
		String email= Sanitize.sanitizeString(request.getParameter("email"));
		String password = request.getParameter("password");
		
		File userLog = new File ("..\\Logs\\UserLogs.txt");
		if (!userLog.exists()) {
			userLog.createNewFile();
		}
		
		FileWriter userLogFileWriter = new FileWriter (userLog, true);
		BufferedWriter userLogBuffWriter = new BufferedWriter(userLogFileWriter);
		PrintWriter userLogPrintWriter = new PrintWriter (userLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);

		if (UserService.verifyUser(email, password)){
			User user = UserService.getUser(email); 
			if (!user.isLockedOut()){
				//login is correct
				System.out.println("Login successful");
				user.resetFailedLogins();
				UserService.updateUser(user, password);
				
				session.setAttribute("isLoggedIn", true);
				session.setAttribute("firstName", UserService.getFirstName(email));
				session.setAttribute("userType", (int)UserService.getUserType(UserService.getUser(email)));
				session.setAttribute("loggedInEmail", UserService.encrypt(email));

				userLogPrintWriter.println("[" + currentDateString + "]" + "[LOGIN][SUCCESS]User: " + email + " successfully logged in");
				userLogPrintWriter.close();
				userLogBuffWriter.close();
				userLogFileWriter.close();
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}else{
				userLogPrintWriter.println("[" + currentDateString + "]" + "[LOGIN][FAIL]User: " + email + " is locked out");
				userLogPrintWriter.close();
				userLogBuffWriter.close();
				userLogFileWriter.close();
				UserService.failedLogin(user);
				response.sendRedirect("/Chipmunk_Store");
				return;
			}
			
			
		}else{
			System.out.println("Login failed");
			User user = UserService.getUser(email); 
			UserService.failedLogin(user);
			session.setAttribute("isLoggedIn", false);

			

			userLogPrintWriter.println("[" + currentDateString + "]" + "[LOGIN][FAIL]User: " + email + " failed to logged in");
			userLogPrintWriter.close();
			userLogBuffWriter.close();
			userLogFileWriter.close();
			response.sendRedirect("/Chipmunk_Store");
			//login is wrong
		}
	}

}
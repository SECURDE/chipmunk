package users;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UserService;

/**
 * Servlet implementation class LoadProducts
 */
@WebServlet("/logout")
public class Logout extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Logout() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String email = UserService.decrypt(session.getAttribute("loggedInEmail").toString());
		if (session != null) {
		    session.invalidate();
		}

		

		File userLog = new File ("..\\Logs\\UserLogs.txt");
		if (!userLog.exists()) {
			userLog.createNewFile();
		}
		FileWriter userLogFileWriter = new FileWriter (userLog, true);
		BufferedWriter userLogBuffWriter = new BufferedWriter(userLogFileWriter);
		PrintWriter userLogPrintWriter = new PrintWriter (userLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		userLogPrintWriter.println("[" + currentDateString + "]" + "[LOGOUT][SUCCESS]User: " + email + " logged out");
		userLogPrintWriter.close();
		userLogBuffWriter.close();
		userLogFileWriter.close();
		response.sendRedirect("/Chipmunk_Store");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}

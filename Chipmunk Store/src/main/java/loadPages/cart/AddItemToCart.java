package cart;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import security.ServletSecurity;
import service.ProductService;

/**
 * Servlet implementation class AddCart
 */
@WebServlet("/addcart")
public class AddItemToCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddItemToCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if (ServletSecurity.isUserLoggedIn(request)){
			HttpSession session = request.getSession();
			
			int itemId = Integer.parseInt((String) request.getParameter("itemid"));
			Item i = ProductService.getItem(itemId);
			
			ArrayList<Item> items = (ArrayList<Item>) session.getAttribute("cartitems");
			ArrayList<Integer> quantities = (ArrayList<Integer>) session.getAttribute("itemquantities");
			ArrayList<Integer> itemIds = (ArrayList<Integer>) session.getAttribute("itemids");
			
			if(items == null){
				items = new ArrayList<Item>();
				quantities = new ArrayList<Integer>();
				itemIds = new ArrayList<Integer>();
			}
			
			if (!(itemIds.contains(i.getId()))){
				System.out.println("Does not exist!");
				items.add(i);
				quantities.add(1);
				itemIds.add(i.getId());
			}
			
			System.out.println("Number of items: " + items.size());
			
			session.setAttribute("itemids", itemIds);
			session.setAttribute("cartitems", items);
			session.setAttribute("itemquantities", quantities);
			session.setAttribute("cartsize", items.size());
			
			request.getRequestDispatcher("basket.jsp").forward(request, response);
		}else{
			//error, tell user to log in
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

package cart;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import security.ServletSecurity;

/**
 * Servlet implementation class DeleteCart
 */
@WebServlet("/deletecart")
public class RemoveFromCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveFromCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		
		if (ServletSecurity.isUserLoggedIn(request)){
			int deleteIndex = Integer.parseInt(request.getParameter("i"));
			ArrayList<Item> items = (ArrayList<Item>) session.getAttribute("cartitems");
			ArrayList<Integer> itemIds = (ArrayList<Integer>) session.getAttribute("itemids");
			ArrayList<Integer> quantities = (ArrayList<Integer>) session.getAttribute("itemquantities");
			
			System.out.println(deleteIndex);
			
			if(items != null){
				if(items.size() >= (deleteIndex + 1)){
					System.out.println("TRUE!");
					items.remove(deleteIndex);
					itemIds.remove(deleteIndex);
					quantities.remove(deleteIndex);
					
					session.setAttribute("cartitems", items);
					session.setAttribute("itemids", itemIds);
					session.setAttribute("cartsize", items.size());
					session.setAttribute("itemquantities", quantities);
				}
				
				if(items.size() == 0){
					session.removeAttribute("cartitems");
					session.removeAttribute("itemids");
					session.removeAttribute("cartsize");
					session.removeAttribute("itemquantities");
				}
			}
			
			request.getRequestDispatcher("basket.jsp").forward(request, response);
		}else{
			//error, tell user to log in
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

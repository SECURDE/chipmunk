package cart;

import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import security.ServletSecurity;

/**
 * Servlet implementation class UpdateCart
 */
@WebServlet("/updatecart")
public class UpdateCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(ServletSecurity.isUserLoggedIn(request)){
			ArrayList<Integer> quantities = (ArrayList<Integer>) session.getAttribute("itemquantities");
			System.out.println(quantities.size());
			
			for(int i = 0; i < quantities.size(); i++){
				String name = "qty" + i;
				System.out.println(request.getParameter(name));
				try{
					if(Integer.parseInt(request.getParameter(name)) > 0){
						quantities.set(i, Integer.parseInt(request.getParameter(name)));
					} else {
						quantities.set(i, 1);
					}
				} catch (NumberFormatException ex){
					System.out.println("INTEGER OVERFLOW/UNDERFLOW!");
					quantities.set(i, 1);
				}
			}
			
			session.setAttribute("itemquantities", quantities);
			request.getRequestDispatcher("basket.jsp").forward(request, response);
		}else{
			//ask user to log in
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

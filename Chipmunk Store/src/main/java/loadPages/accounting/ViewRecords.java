package accounting;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Category;
import model.Item;
import model.Sanitize;
import model.User;
import security.ServletSecurity;
import service.ProductService;
import service.PurchaseService;
import service.UserService;

@WebServlet("/view_financial_records")
public class ViewRecords extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public ViewRecords() {
		super();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = ServletSecurity.getLoggedInUser(request);
		
		if (ServletSecurity.isUserLoggedIn(request) && user.isFinancialOfficer()){
			request.setAttribute("all", PurchaseService.getAllTotalSales());
			request.setAttribute("category", PurchaseService.getSalesPerProductType());
			request.setAttribute("item", PurchaseService.getSalesPerProduct());
			
			request.getRequestDispatcher("/accountingManager.jsp").forward(request, response);
		}else{
			response.sendRedirect("error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
	}
}


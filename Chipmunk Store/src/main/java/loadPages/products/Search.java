package products;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Item;
import model.Sanitize;
import service.ProductService;

/**
 * Servlet implementation class LoadProducts
 */
@WebServlet("/search")
public class Search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Search() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String search = Sanitize.sanitizeString(request.getParameter("search"));
		System.out.println("YOU ATTEMPTED TO SEARCH FOR: "+ search);
		String category = Sanitize.sanitizeString(request.getParameter("c"));
		List<Item> tempProducts = ProductService.getProductsByCategory(category);
		List<Item> products = new ArrayList<Item>();
		for (Item item : tempProducts) {
			if(item.getName().toLowerCase().contains(search.toLowerCase()))
			{
		    System.out.println("Yeah it matches: "+item.getName());
		    products.add(item);
			}
			else{
				System.out.println("nope"+item.getName());
			}
		}
		
		request.setAttribute("c", category);
		request.setAttribute("search", search);
		request.setAttribute("products", products);
		request.getRequestDispatcher("/product_list.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

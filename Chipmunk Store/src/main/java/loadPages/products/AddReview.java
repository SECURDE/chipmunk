package products;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Item;
import model.Purchase;
import model.Review;
import model.Sanitize;
import model.Transaction;
import model.User;
import security.ServletSecurity;
import service.ProductService;
import service.PurchaseService;
import service.TransactionService;
import service.UserService;

/**
 * Servlet implementation class AddReview
 */
@WebServlet("/AddReview")
public class AddReview extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddReview() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("YOU CALLED ADD REVIEW GET");
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
System.out.println("YOU CALLED ADD REVIEW POST");
		User user = ServletSecurity.getLoggedInUser(request);
		int itemId = Integer.parseInt(request.getParameter("p"));
		File reviewLog = new File ("..\\Logs\\ReviewLogs.txt");
		if (!reviewLog.exists()) {
			reviewLog.createNewFile();
		}
		
		FileWriter reviewLogFileWriter = new FileWriter (reviewLog, true);
		BufferedWriter reviewLogBuffWriter = new BufferedWriter(reviewLogFileWriter);
		PrintWriter reviewLogPrintWriter = new PrintWriter (reviewLogBuffWriter);
		
		DateFormat formatCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date currentDate = Calendar.getInstance().getTime();
		String currentDateString = formatCurrentDate.format(currentDate);
		
		if (ServletSecurity.isUserLoggedIn(request)){
			Item i = ProductService.getItem(itemId);
			String content = Sanitize.sanitizeString(request.getParameter("c"));
			int rating = Integer.parseInt(request.getParameter("r"));
			System.out.println("itemID: "+itemId);
			System.out.println("content: "+content);
			System.out.println("rating: "+rating);
			if (i.equals(Item.NONE) || i == null){
				System.out.println("THERE IS NONE");
				System.out.println("CANNOT ADD REVIEW BECAUSE NO PURCHASE");
				reviewLogPrintWriter.println("[" + currentDateString + "]" + "[ADD REVIEW][FAIL]User: " + user.getEmail() +
						" tried to review item: " + i.getId() +
						" that does not exist");
				reviewLogPrintWriter.close();
				reviewLogBuffWriter.close();
				reviewLogFileWriter.close();
				response.sendRedirect("error");
			} else if (!user.hasPurchased(i)) {
				System.out.println("CANNOT ADD REVIEW BECAUSE NO PURCHASE");
				reviewLogPrintWriter.println("[" + currentDateString + "]" + "[ADD REVIEW][FAIL]User: " + user.getEmail() +
						" tried to review item: " + i.getId() +
						" without purchasing");
				reviewLogPrintWriter.close();
				reviewLogBuffWriter.close();
				reviewLogFileWriter.close();
				response.sendRedirect("error");
			} else{
				
				System.out.println("WILL CREATE:user:" +user+ " i: "+i +" content: "+ content +" rating: "+rating);
				Review newReview = new Review(user, i, content, rating);
				i.review(newReview);
				ProductService.updateItem(i);
				request.setAttribute("product", i);
				reviewLogPrintWriter.println("[" + currentDateString + "]" + "[ADD REVIEW][SUCCESS]User: " + user.getEmail() +
						" added review: " + content +
						" to item: " + i.getId() + 
						" with rating: " + rating);
				reviewLogPrintWriter.close();
				reviewLogBuffWriter.close();
				reviewLogFileWriter.close();
				request.getRequestDispatcher("/product_detail.jsp").forward(request, response);
			}
		}else{
			//error, ask user to log in
			reviewLogPrintWriter.println("[" + currentDateString + "]" + "[ADD REVIEW][FAIL]Annonymous user tried to add review.");
			reviewLogPrintWriter.close();
			reviewLogBuffWriter.close();
			reviewLogFileWriter.close();
			request.getRequestDispatcher("register.jsp").forward(request, response);
		}
		
	}

}

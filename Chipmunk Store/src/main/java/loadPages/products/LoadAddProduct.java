package products;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Item;
import model.User;
import security.ServletSecurity;
import service.ProductService;
import service.UserService;

/**
 * Servlet implementation class LoadAddProduct
 */
@WebServlet("/products/new")
public class LoadAddProduct extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadAddProduct() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user =  ServletSecurity.getLoggedInUser(request);
		if (ServletSecurity.isUserLoggedIn(request) && user.isProductManager()){
			List<String> categories = ProductService.getAllCategoryNames();
			request.setAttribute("categories", categories);
			request.getRequestDispatcher("/add_item.jsp").forward(request, response);
		}else{
			response.sendRedirect("error");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}

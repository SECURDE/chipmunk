package service;

import java.util.ArrayList;
import java.util.List;

import model.Category;
import model.Item;
import model.Purchase;
import model.User;
import repository.CategoryRepository;
import repository.ItemRepository;
import repository.PurchaseRepository;
import repository.TransactionRepository;

public class PurchaseService {
	static PurchaseRepository p = new PurchaseRepository();
	static TransactionRepository t = new TransactionRepository();
	static CategoryRepository c = new CategoryRepository();
	static ItemRepository i = new ItemRepository();

	public static Purchase getPurchase(int id) {
		return p.selectRow(id);
	}

	public static Purchase getPurchaseByUserAndItem(int idUser, int idItem) {
		return p.getPurchaseByUserAndItem(idUser, idItem);
	}

	public static void createPurchase(Purchase purchase) {
		p.insert(purchase);
	}

	public static void updatePurchase(Purchase purchase) {
		p.update(purchase);
	}

	public static List<Purchases> getAllTotalSales() {
		System.out.println("Called getAllTotalSales()" );
		List<Purchase> purchase = p.selectAll();
		List<Purchases> purchases = new ArrayList<Purchases>();
		int total = 0;
		int quantity = 0;
		for (Purchase p : purchase) {
			Item item = p.getItem();
			total += p.getQuantity() * item.getPrice();
			quantity += p.getQuantity();
			System.out.println("TOTAL ");
		}
		purchases.add(new Purchases("Total", quantity, "-", total));
		return purchases;
	}

	public static List<Purchases> getSalesPerProductType() {
		System.out.println("Called getSalesPerProductType()" );
		List<Purchase> purchase = p.selectAll();
		List<Purchases> purchases = new ArrayList<Purchases>();
		List<Category> category = c.selectAll();
		for (Category cat : category) {
			float total = 0;
			int quantity = 0;
			for (Purchase p : purchase) {
				Item item = p.getItem();
				if (item.getCategory().getName().equals(cat.getName())) {
					total += p.getQuantity() * item.getPrice();
					quantity += p.getQuantity();
				}
			}
			purchases.add(new Purchases(cat.getName(), quantity, "-", total));
		}
		return purchases;
	}

	public static List<Purchases> getSalesPerProduct() {
		System.out.println("Called getSalesPerProduct()" );
		List<Purchase> purchase = p.selectAll();
		List<Purchases> purchases = new ArrayList<Purchases>();
		List<Item> items = i.selectAll();
		for (Item item : items) {
			int total = 0;
			int quantity = 0;
			for (Purchase p : purchase) {
				Item purchaseItem = p.getItem();
				if (purchaseItem.getName().equals(item.getName())) {
					total += p.getQuantity() * purchaseItem.getPrice();
					quantity += p.getQuantity();
				}
			}
			purchases.add(new Purchases(item.getName(), quantity, String.valueOf(item.getPrice()), total));
		}
		return purchases;
	}
}

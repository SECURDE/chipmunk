package service;

public class Purchases {
	String name;
	int quantity;
	String price;
	float total;

	public Purchases(String name, int quantity, String price, float total) {
		super();
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.total = total;
	}

	public String getName() {
		return name;
	}

	public int getQuantity() {
		return quantity;
	}

	public String getPrice() {
		return price;
	}

	public float getTotal() {
		return total;
	}
}
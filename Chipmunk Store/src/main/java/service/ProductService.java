package service;

import java.util.ArrayList;
import java.util.List;

import model.Category;
import model.Item;
import repository.CategoryRepository;
import repository.ItemRepository;


public class ProductService {
	static CategoryRepository c = new CategoryRepository();
	static ItemRepository i = new ItemRepository();
	
	public static List<Item> getProductsByCategory(String category) {
		Category cat = c.selectRow(category);
		if (cat.equals(Category.NONE)){
			return new ArrayList<Item>();
		}else{
			System.out.println("List size: " + cat.getItems().size());
			return cat.getItems();
		}
	}
	
	public static List<Item> getProductsByName (String name) {
		return i.selectByName(name);
	}
	
	public static Item getItem(int id) {
		return i.selectRow(id);
	}
	
	public static void createItem(Item item) {
		i.insert(item);
	}
	
	public static void updateItem(Item item) {
		i.update(item);
	}
	
	public static void deleteItem(Item item) {
		item.discontinue();
		i.update(item);
	}
	public static Category getCategoryByName(String cat) {
		return c.selectRow(cat);
	}
	public static List<String> getAllCategoryNames() {
		List<Category> categories = c.selectAll();
		List<String> cNames = new ArrayList<String>();
		for (Category cat : categories){
			cNames.add(cat.getName());
		}
		return cNames;
	}
}

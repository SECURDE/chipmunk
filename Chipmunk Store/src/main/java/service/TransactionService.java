package service;

import java.util.ArrayList;
import java.util.List;

import model.Transaction;
import model.User;
import repository.TransactionRepository;
import repository.UserRepository;

public class TransactionService {

	static TransactionRepository t = new TransactionRepository();
	static UserRepository u = new UserRepository();
	
	public static Transaction getTransaction(int id){
		return t.selectRow(id);
	}
	
	public static List<Transaction> getTransactionsByUser(int idUser){
		User user = u.selectRow(idUser);
		
		if (user.equals(User.NONE)){
			return new ArrayList<Transaction>();
		} else {
			System.out.println("List of transactions of a user: " + user.getTransactions().size());
			return user.getTransactions();
		}
	}
	
	
	public static void createTransaction(Transaction transaction){
		t.insert(transaction);
	}
	
	public static void updateTransaction(Transaction transaction){
		t.update(transaction);
	}
	
	public static void prepareTransaction(Transaction transaction){
		transaction.setPreparing();
		t.update(transaction);
	}
	
	public static void holdTransaction(Transaction transaction){
		transaction.setHold();
		t.update(transaction);
	}
	
	public static void cancelTransaction(Transaction transaction){
		transaction.setCancelled();
		t.update(transaction);
	}
	
	public static void receiveTransaction(Transaction transaction){
		transaction.setReceived();
		t.update(transaction);
	}
	
}

package security;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import model.Item;
import model.User;
import service.UserService;

public class ServletSecurity {
	public static final String ATTRIBUTE_EMAIL = "loggedInEmail";
	public static final String ATTRIBUTE_CART = "cart";

	public static String getDecryptedLoggedInEmail(HttpServletRequest request){
		HttpSession session = request.getSession();
		return UserService.decrypt((String)session.getAttribute(ATTRIBUTE_EMAIL));
	
	}
	public static User getLoggedInUser(HttpServletRequest request){
		String currentEmail = getDecryptedLoggedInEmail(request);
		User user = UserService.getUser(currentEmail);
		return user;
	}
	public static boolean isUserLoggedIn(HttpServletRequest request) {
		User u = getLoggedInUser(request);
		if (u != null && !u.equals(User.NONE)){
			return true;
		}else{
			return false;
		}
	}
	
	
	
	
}

<%@ page import="model.User" %>
<%@ page import="security.ServletSecurity" %>
<%@ page import="service.PurchaseService" %>
<% 
User user = ServletSecurity.getLoggedInUser(request);
if (!ServletSecurity.isUserLoggedIn(request) || !user.isFinancialOfficer()){
	response.sendRedirect("/Chipmunk_Store");
}%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">


<title>Shoes</title>

<meta name="keywords" content="">

<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>


<link rel="shortcut icon" href="favicon.png">



</head>

<body>

	<jsp:include page="js/navBar.jsp" />

	<!-- *** NAVBAR END *** -->

	<div id="all">

		<div id="content">
			<div class="container">


				<div class="col-md-9">

					<div class="box">
						<h1>Financial Records</h1>
					</div>

					<div class="row products">
						<div class="col-md-3">
							<div class="panel panel-default sidebar-menu">

								<div class="panel-heading">
									<h3 class="panel-title">Filter Records</h3>
								</div>

								<div class="panel-body">

									<form>
										<div class="form-group">
											<input type="radio" name="filterSales" value="Total sales" onclick="showAll()"> Total sales<br>
											<br> <input type="radio" name="filterSales" value="Sales per product type" onclick = "showCategory()"> Sales per product type<br>
											<br> <input type="radio" name="filterSales" value="Sales per product" onclick="showItem()"> Sales per product
										</div>
									</form>
								</div>
							</div>
						</div>


						<div class="col-md-9" style="width: 75%;">
							<div class="box">
								<table class="table" id = "allpurchases">
									<thead>
										<tr>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="product" items="${all}">
											<tr>
												<td>PHP ${product.total}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<table class="table" id = "category">
									<thead>
										<tr>
											<th>Product</th>
											<th>Total</th>

										</tr>
									</thead>
									<tbody>
										<c:forEach var="product" items="${category}">
											<tr>
												<td>${product.name}</td>
												<td>PHP ${product.total}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
								<table class="table" id = "item">
									<thead>
										<tr>
											<th>Product</th>
											<th>Quantity</th>
											<th>Unit price</th>
											<th>Total</th>

										</tr>
									</thead>
									<tbody>
										<c:forEach var="product" items="${item}">
											<tr>
												<td>${product.name}</td>
												<td>${product.quantity}</td>
												<td>PHP ${product.price}</td>
												<td>PHP ${product.total}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<!-- /.container -->
	</div>
	<!-- /#content -->




	<!-- *** COPYRIGHT ***
 _________________________________________________________ -->
	<div id="copyright">
		<div class="container">
			<div class="col-md-6">
				<p class="pull-left">© 2016 Chipmunk & Friends.</p>

			</div>
			<div class="col-md-6">
				<p class="pull-right">
					Template by <a href="http://bootstrapious.com/e-commerce-templates">Bootstrapious</a> with support from <a href="https://kakusei.cz">Kakusei</a>
					<!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
				</p>
			</div>
		</div>
	</div>
	<!-- *** COPYRIGHT END *** -->




	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap-hover-dropdown.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/front.js"></script>
	<script>
	
	showAll = function(){
		$('#allpurchases').show();
		$('#category').hide();
		$('#item').hide();
	}
	
	showCategory = function(){
		$('#allpurchases').hide();
		$('#category').show();
		$('#item').hide();
	}
	
	showItem = function(){
		$('#allpurchases').hide();
		$('#category').hide();
		$('#item').show();
	}
	
	showAll();
	</script>






</body>

</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">


    <title>
        ${product.name}
    </title>


<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">



</head>

<body>
	<jsp:include page="js/navBar.jsp" />
	<!-- *** NAVBAR END *** -->

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li><a href="#">${product.category.name}</a></li>
						<li>${product.name}</li>
					</ul>

				</div>

				<div class="col-md-1"></div>

				<div class="col-md-9" style="width: 85%;">

					<div class="row" id="productMain">
						<div class="col-sm-6">
							<div id="mainImage">
								<img src="img/detailbig1.jpg" alt="" class="img-responsive">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="box">
								<h1 class="text-center">${product.name}</h1>
								<p class="goToDescription">
									<a href="#details" class="scroll-to">Scroll to product
										details, material & care and sizing</a>
								</p>
								<p class="price">PHP ${product.price}</p>

								<p class="text-center buttons">

									<c:choose>
										<c:when test="${sessionScope.isLoggedIn}">
											<form method="POST" action="addcart">
												<input type="hidden" name="itemid" value="${product.id}">
												<button type="submit" class="btn btn-primary">
													<i class="fa fa-shopping-cart"></i> Add to cart
												</button>
											</form>

										</c:when>
										<c:otherwise>
                                		Login to Buy Stuff
											</c:otherwise>
									</c:choose>
								</p>


							</div>

							<div class="row" id="thumbs">
								<div class="col-xs-4">
									<a href="img/detailbig1.jpg" class="thumb"> <img
										src="img/detailsquare.jpg" alt="" class="img-responsive">
									</a>
								</div>
								<div class="col-xs-4">
									<a href="img/detailbig2.jpg" class="thumb"> <img
										src="img/detailsquare2.jpg" alt="" class="img-responsive">
									</a>
								</div>
								<div class="col-xs-4">
									<a href="img/detailbig3.jpg" class="thumb"> <img
										src="img/detailsquare3.jpg" alt="" class="img-responsive">
									</a>
								</div>
							</div>
						</div>

					</div>


					<div class="box" id="details">
						<h4>Product description</h4>
						<p>${product.description}</p>
					</div>


					<c:if test="${sessionScope.isLoggedIn}">
					<div class="box" id="writeReviews">
                       <h4>Write a Review</h4>
                       <form action="AddReview" method="Post">
                       <input type="hidden" name="p" value="${product.id}">
												
                       <select name="r" class="form-control" style="width: 10%">
										<option value=1>1</option>
									    <option value=2>2</option>
									    <option value=3>3</option>
									    <option value=4>4</option>
									    <option value=5>5</option>
									</select>
                       <textarea name="c" rows="3" cols="40" class="form-control"></textarea>
                       
                      	 <input type="submit">
                       </form>
                       
                    </div>
					</c:if>

 					<div class="box" id="reviews">
                       <h4>Reviews</h4>
                       <c:forEach items="${product.reviews }" var="review">
                       
                       	<p><b>${review.user}</b> gives this item a rating of ${review.rating}: ${review.content}</p>
                       </c:forEach>
                       
                    </div>
				</div>
				<!-- /.col-md-9 -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***
 _________________________________________________________ -->
		<div id="footer" data-animate="fadeInUp">
			<div class="container">
				<div class="row">
				</div>
			</div>
		</div>

		<div id="copyright">
			<div class="container">
				<div class="col-md-6">
					<p class="pull-left">© 2015 Your name goes here.</p>

				</div>
				<div class="col-md-6">
					<p class="pull-right">
						Template by <a
							href="http://bootstrapious.com/e-commerce-templates">Bootstrapious</a>
						with support from <a href="https://kakusei.cz">Kakusei</a>
						<!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
					</p>
				</div>
			</div>
		</div>
		<!-- *** COPYRIGHT END *** -->



	</div>
	<!-- /#all -->




	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap-hover-dropdown.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/front.js"></script>

</body>

</html>
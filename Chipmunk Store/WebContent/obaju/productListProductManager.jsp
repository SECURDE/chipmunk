<%@ page import="model.User" %>
<%@ page import="security.ServletSecurity" %>
<%@ page import="service.PurchaseService" %>
<% 
User user = ServletSecurity.getLoggedInUser(request);
if (!ServletSecurity.isUserLoggedIn(request) || !user.isProductManager()){
	response.sendRedirect("/Chipmunk_Store");
}%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">


<title>${c} | Chipmunk Store</title>

<meta name="keywords" content="">

<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>


<link rel="shortcut icon" href="favicon.png">
</head>

<body>
	<!-- *** TOPBAR ***
 _________________________________________________________ -->

	<!-- *** TOP BAR END *** -->

	<!-- *** NAVBAR ***
 _________________________________________________________ -->



 <jsp:include page="js/navBar.jsp"/>
	<!-- *** NAVBAR END *** -->

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">
					<ul >
								<form class="navbar-form" role="search" action="search">
								<div class="input-group">
								<input type="hidden" name="c" value="${c}">
									<input type="text" class="form-control" placeholder="Search" name="search">
									<span class="input-group-btn">

										<button type="submit" class="btn btn-primary">
											<i class="fa fa-search"></i>
										</button>

									</span>
								</div>
							</form>
					</ul>
					
					<a href='products/new'><button class='btn btn-primary btn-lg pull-right' style="height: 120px;">+ &nbsp Create A New Product</button></a>
				</div>

				<div class="col-md-9">
					<div class="row products">
						<c:forEach var ="product" items="${products}">
						<c:if test="${product.getStatus()!= 'DISCONTINUED'}">
							<div class="col-md-4 col-sm-6">
								<div class="product">
									<div class="flip-container">
										<div class="flipper">
											<div class="front">
												<a href="detail.html"> <img src="img/product2.jpg" alt="" class="img-responsive">
												</a>
											</div>
											<div class="back">
												<a href="detail.html"> <img src="img/product2_2.jpg" alt="" class="img-responsive">
												</a>
											</div>
										</div>
									</div>
									<a href="detail.html" class="invisible"> <img src="img/product2.jpg" alt="" class="img-responsive">
									</a>
									<div class="text">
										<h3>
											<a href="detail.html">${product.name}</a>
										</h3>
										<p class="price">PHP ${product.price}</p>
										<p class="buttons">
											<a href="product?p=${product.id}" class="btn btn-default">View detail</a>
											<a href="products/edit?p=${product.id}" class="btn btn-default">Edit</a>
											<a href="delete_product?p=${product.id}" class="btn btn-default">DELETE</a>
											<form method = "POST" action = "addcart">
												<input type="hidden" name="itemid" value="${product.id}"/>
												<button type="submit" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Add to cart</button>
											</form>	
										
										</p>
									</div>
									<!-- /.text -->
								</div>
							</div><!-- /.product -->
							</c:if>
						</c:forEach>
					</div>
					<!-- /.col-md-9 -->

					<div class="col-md-3">


					</div>
				</div>
			</div>

		</div>
		<!-- /.container -->
	</div>
	<!-- /#content -->


	<!-- *** FOOTER ***
 _________________________________________________________ -->
	<div id="footer" data-animate="fadeInUp">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<h4>Pages</h4>

					<ul>
						<li><a href="text.html">About us</a></li>
						<li><a href="text.html">Terms and conditions</a></li>
						<li><a href="faq.html">FAQ</a></li>
						<li><a href="contact.html">Contact us</a></li>
					</ul>

					<hr>

					<h4>User section</h4>

					<ul>
						<li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
						<li><a href="register.html">Regiter</a></li>
					</ul>

					<hr class="hidden-md hidden-lg hidden-sm">

				</div>
				<!-- /.col-md-3 -->

				<div class="col-md-3 col-sm-6">

					<h4>Top categories</h4>

					<h5>Men</h5>

					<ul>
						<li><a href="category.html">T-shirts</a></li>
						<li><a href="category.html">Shirts</a></li>
						<li><a href="category.html">Accessories</a></li>
					</ul>

					<h5>Ladies</h5>
					<ul>
						<li><a href="category.html">T-shirts</a></li>
						<li><a href="category.html">Skirts</a></li>
						<li><a href="category.html">Pants</a></li>
						<li><a href="category.html">Accessories</a></li>
					</ul>

					<hr class="hidden-md hidden-lg">

				</div>
				<!-- /.col-md-3 -->

				<div class="col-md-3 col-sm-6">

					<h4>Where to find us</h4>

					<p>
						<strong>Obaju Ltd.</strong> <br>13/25 New Avenue <br>New Heaven <br>45Y 73J <br>England <br> <strong>Great Britain</strong>
					</p>

					<a href="contact.html">Go to contact page</a>

					<hr class="hidden-md hidden-lg">

				</div>
				<!-- /.col-md-3 -->

				<div class="col-md-3 col-sm-6">

					<h4>Get the news</h4>

					<p class="text-muted">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

					<form>
						<div class="input-group">

							<input type="text" class="form-control"> <span class="input-group-btn">

								<button class="btn btn-default" type="button">Subscribe!</button>

							</span>

						</div>
						<!-- /input-group -->
					</form>

					<hr>

					<h4>Stay in touch</h4>

					<p class="social">
						<a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a> <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a> <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a> <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a> <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
					</p>


				</div>
				<!-- /.col-md-3 -->

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->
	</div>
	<!-- /#footer -->
	<div id="copyright">
		<div class="container">
			<div class="col-md-6">
				<p class="pull-left">© 2015 Your name goes here.</p>

			</div>
			<div class="col-md-6">
				<p class="pull-right">
					Template by <a href="http://bootstrapious.com/e-commerce-templates">Bootstrapious</a> with support from <a href="https://kakusei.cz">Kakusei</a>
					<!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
				</p>
			</div>
		</div>
	</div>
	<!-- *** COPYRIGHT END *** -->



	</div>
	<!-- /#all -->




	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap-hover-dropdown.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/front.js"></script>






</body>

</html>
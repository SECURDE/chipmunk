<%@ page import="model.User" %>
<%@ page import="security.ServletSecurity" %>
<%@ page import="service.PurchaseService" %>
<% 
User user = ServletSecurity.getLoggedInUser(request);
if (!ServletSecurity.isUserLoggedIn(request) || !user.isAdmin()){
	response.sendRedirect("/Chipmunk_Store");
}%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">


<title>Shoes</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>


<link rel="shortcut icon" href="favicon.png">



</head>

<body>
	<!-- *** TOPBAR ***
 _________________________________________________________ -->

	<!-- *** TOP BAR END *** -->

	<!-- *** NAVBAR ***
 _________________________________________________________ -->


 <jsp:include page="js/navBar.jsp"/>


	<!-- *** NAVBAR END *** -->

	<div id="all">

		<div id="content">
			<div class="container">


				<div class="col-md-9">

					<div class="box">
						<h1>Administrator Panel</h1>
					</div>

					<div class="row products">

						<div class="col-md-6" style="width: 45%;">

							<div class="box">
								<h2>Spawn a new account</h2>
								<br>

								<form action="register" method="post">
									<div class="form-group">
										Username: <input type="text" name="username"> <br>
										<br> Password: <input type="text" name="password">
										<br> <br> <br> Account Type:
										<div class="form-group">
											<input type="radio" name="userType" value="1">
											Regular User<br> <input type="radio" name="userType"
												value="2"> Product Manager<br> <input
												type="radio" name="userType" value="3"> Accounting
											Manager
										</div>
									</div>
									<button class='btn btn-primary'>Spawn Account</button>
								</form>
								<br> <br>

							</div>
						</div>
						<div class="col-md-3" style="width: 45%;">
							<div class="box">

								<table class="table">
									<thead>
										<tr>
											<th>Email</th>
											<th>Failed Logins</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="currentUser" items="${lockedUsers}">
											<tr>

												<td>${currentUser.getEmail()}</td>
												<td>${currentUser.getFailedLogins()}</td>
												<td>
												<form method="Post" action ="AbsolveUser?email=${currentUser.getEmail()}">
												<input type="submit" value ="Unlock User">
												</form>
												</td>

											</tr>
										</c:forEach>

									</tbody>
								</table>
							</div>
						</div>
						<!-- /.col-md-9 -->

					</div>
					<div class="col-md-3">
						<!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->




					</div>
				</div>


			</div>

		</div>
		<!-- /.container -->
	</div>
	<!-- /#content -->




	<!-- *** COPYRIGHT ***
 _________________________________________________________ -->
	<div id="copyright">
		<div class="container">
			<div class="col-md-6">
				<p class="pull-left">© 2016 Chipmunk & Friends.</p>

			</div>
			<div class="col-md-6">
				<p class="pull-right">
					Template by <a href="http://bootstrapious.com/e-commerce-templates">Bootstrapious</a>
					with support from <a href="https://kakusei.cz">Kakusei</a>
					<!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
				</p>
			</div>
		</div>
	</div>
	<!-- *** COPYRIGHT END *** -->



	</div>
	<!-- /#all -->




	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
	<script src="js/jquery-1.11.0.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.cookie.js"></script>
	<script src="js/waypoints.min.js"></script>
	<script src="js/modernizr.js"></script>
	<script src="js/bootstrap-hover-dropdown.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/front.js"></script>






</body>

</html>
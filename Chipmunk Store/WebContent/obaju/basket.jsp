<%@ page import="model.User" %>
<%@ page import="security.ServletSecurity" %>
<%@ page import="service.PurchaseService" %>
<% 
User user = ServletSecurity.getLoggedInUser(request);
if (!ServletSecurity.isUserLoggedIn(request)){
	response.sendRedirect("/Chipmunk_Store");
}%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Obaju e-commerce template">
    <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
    <meta name="keywords" content="">

    <title>
        Obaju : e-commerce template
    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>
    
    <script type="text/javascript">

    function submitForm()
    {
    	var cardName = document.getElementById("name").value;
    	var cardNumber = document.getElementById("cardNum").value;
    	var securityNumber = document.getElementById("securityNum").value;
    	var year = document.getElementById("year").value;
    	
    	var isValidCardName, isValidCardNumber, isValidSecurityNumber, isValidYear;
    	
    	var nameRegEx = /^[a-zA-Z ]+$/;
    	if(nameRegEx.test(cardName)){
    		isValidCardName = true;
    	} else {
    		isValidCardName = false;
    	}
    	
    	if(cardNumber.length >= 16 && cardNumber.length <= 20 && (!isNaN(cardNumber) && isFinite(cardNumber))){
    		isValidCardNumber = true;
    	} else {
    		isValidCardNumber = false;
    	}
    	
    	if(securityNumber.length == 3 && (!isNaN(securityNumber) && isFinite(securityNumber))){
    		isValidSecurityNumber = true;
    	} else {
    		isValidSecurityNumber = false;
    	}
    	
    	if(year.length == 4 && (!isNaN(year) && isFinite(year))){
    		isValidYear = true;
    	} else {
    		isValidYear = false;
    	}
    	
    	if (isValidCardName && isValidCardNumber && isValidSecurityNumber && isValidYear){
    		document.getElementById("cartForm").action = "addtransaction";
        	document.getElementById("cartForm").submit;
    	} else {
    		alert("Invalid inputs. Please try again.");
    		document.getElementById("cartForm").action = "basket.jsp";
        	document.getElementById("cartForm").submit;
    	}
    	
    }
 
    </script>

    <link rel="shortcut icon" href="favicon.png">



</head>

<body>
    <jsp:include page="js/navBar.jsp" />
    <!-- *** NAVBAR END *** -->

    <div id="all">

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="index.jsp">Home</a>
                        </li>
                        <li>Shopping cart</li>
                    </ul>
                </div>

                <div class="col-md-9" id="basket" style="width: 75%;">

                    <div class="box">

                        <form method="POST" id="cartForm">

                            <h1>Shopping cart</h1>
                            <p class="text-muted">You currently have <c:choose><c:when test="${not empty cartsize}">${cartsize}</c:when><c:otherwise>0</c:otherwise></c:choose> item(s) in your cart.</p>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Product</th>
                                            <th>Quantity</th>
                                            <th>Unit price</th>
                                            <th>Discount</th>
                                            <th colspan="2">Total</th>
                                        </tr>
                                    </thead>
                                    
                             	<!-- Products -->
                                    <c:if test="${not empty cartsize}">
                                    	<c:set var="total" value="${0}" />
                                    	<tbody>
	                                    	<c:forEach var="product" items="${cartitems}" varStatus="counter">
	                                    		<tr>
	                                    			<td>
	                                    				<a href="product?p=${product.id}">
	                                    					<img src="img/detailsquare.jpg" alt="${product.name}">
	                                    				</a>
	                                    			</td>
	                                    			<td>
	                                    				<a href="product?p=${product.id}">${product.name}</a>
	                                    			</td>
	                                    			<td>
	                                    				<input type="number" name="qty${counter.index}" value="${itemquantities[counter.index]}" class="form-control" style="width: 80px;">
	                                    			</td>
	                                    			<td>
	                                    				PHP <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${product.price}" />
	                                    			</td>
	                                    			<td>
	                                    				PHP 0.00
	                                    			</td>
	                                    			<td>
	                                    				PHP <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${product.price * itemquantities[counter.index]}" />
	                                    				<c:set var="total" value="${total + (product.price * itemquantities[counter.index])}" />
	                                    			</td>
	                                    			<td>
	                                    				<a href="deletecart?i=${counter.index}"><i class="fa fa-trash-o"></i></a>
	                                    			</td>
	                                    		</tr>
	                                    	</c:forEach>
	                                    </tbody>
	                                    <tfoot>
	                                    	<tr>
	                                    		<th colspan="5">Total</th>
	                                    		<th colspan="2">PHP <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${total}" /></th>
	                                    	</tr>
	                                    </tfoot>
	                                    <input type="hidden" id="grandtotal" name="grandtotal" value="${total + (total * 0.12) + (total * 0.60)}" />
                                    </c:if>
                                <!-- End Products -->
                                
                                </table>
		        				
		        				<c:if test="${not empty cartsize}">
			        				<!-- Credit Card Form -->
	                                <div class="container">
		                  					<fieldset>
		                  						Please fill in your credit card info:</br></br>
		                  						
		                  						<!-- Card Holder Name -->
		                  						<div class="form-group">
		                  							<label class="col-md-2 control-label">Card Holder Name</label>
		                  							<div class="col-md-4 inputGroupContainer">
		                  								<div class="input-group">
		                  									<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		                  									<input name="name" id="name" placeholder="Credit Card Holder Name" class="form-control" type="text">
		                  								</div>
		                  							</div>
		                  						</div>
		                  						</br>
		                  						
		                  						<!-- Credit Card Number -->
		                  						<div class="form-group">
		                  							<label class="col-md-2 control-label">Credit Card Number</label>
		                  							<div class="col-md-4 inputGroupContainer" >
		                  								<div class="input-group">
		                  									<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
		                  									<input name="cardNum" id="cardNum" placeholder="Credit Card Number" class="form-control" type="text">
		                  								</div>
		                  							</div>
		                  						</div>
		                  						</br>
		                  						
		                  						<!-- Security Number -->
		                  						<div class="form-group">
		                  							<label class="col-md-2 control-label">Security Number</label>
		                  							<div class="col-md-4 inputGroupContainer">
		                  								<div class="input-group">
		                  									<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
		                  									<input name="securityNum" id="securityNum" placeholder="Security Number" class="form-control" type="text">
		                  								</div>	
		                  							</div>
		                  						</div>
		                  						</br>
		                  						
		                  						<!-- Expiration Date -->
		                  						<div class="form-group">
		                  							<label class="col-md-2 control-label">Expiration Date</label>
		                  							<div class="col-md-4 inputGroupContainer">
		                  								<div class="input-group">
		                  									<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
		                  									<select name="month" class="form-control selectpicker">
		                  										<option value="1">01</option>
		                  										<option value="2">02</option>
		                  										<option value="3">03</option>
		                  										<option value="4">04</option>
		                  										<option value="5">05</option>
		                  										<option value="6">06</option>
		                  										<option value="7">07</option>
		                  										<option value="8">08</option>
		                  										<option value="9">09</option>
		                  										<option value="10">10</option>
		                  										<option value="11">11</option>
		                  										<option value="12">12</option>
		                  									</select>
		                  									<span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
		                  									<input name="year" id="year" placeholder="yyyy" class="form-control" type="text">
		                  								</div>
		                  							</div>
		                  						</div>
		                  					</fieldset>
	                                </div>
	                                <!-- /Credit Card Form -->
                                </c:if>
                                
                            </div>
                            <!-- /.table-responsive -->

                            <div class="box-footer">
                                <div class="pull-left">
                                    <a href="index.jsp" class="btn btn-default"><i class="fa fa-chevron-left"></i>Back to Home</a>
                                </div>
                                
                                <c:if test="${not empty cartsize}">
	                                <div class="pull-right">
	                                    <button type="submit" value="update" onClick="this.form.action='updatecart'; this.form.submit()" class="btn btn-default"><i class="fa fa-refresh"></i> Update basket</button></a>
	                                    <button type="submit" value="confirm" onClick="submitForm()" class="btn btn-primary">Confirm Purchase<i class="fa fa-chevron-right"></i></button>
	                                </div>
	                            </c:if>
                            </div>

                        </form>
                        

                    </div>
                    <!-- /.box -->


                </div>
                <!-- /.col-md-9 -->

                <div class="col-md-3">
                    <div class="box" id="order-summary">
                        <div class="box-header">
                            <h3>Order summary</h3>
                        </div>
                        <p class="text-muted">Shipping and additional costs are calculated based on the values you have entered.</p>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Order subtotal</td>
                                        <th>PHP <c:choose><c:when test="${not empty cartsize}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${total}" /></c:when><c:otherwise>0.00</c:otherwise></c:choose></th>
                                    </tr>
                                    <tr>
                                        <td>Shipping and handling</td>
                                        <c:set var="shipping" value="${total * 0.60}" />
                                        <th>PHP <c:choose><c:when test="${not empty cartsize}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${shipping}" /></c:when><c:otherwise>0.00</c:otherwise></c:choose></th>
                                    </tr>
                                    <tr>
                                        <td>Tax</td>
                                        <c:set var="tax" value="${total * 0.12}" />
                                        <th>PHP <c:choose><c:when test="${not empty cartsize}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${tax}" /></c:when><c:otherwise>0.00</c:otherwise></c:choose></th>
                                    </tr>
                                    <tr class="total">
                                        <td>Total</td>
                                        <c:set var="grandtotal" value="${total + shipping + tax}" />
                                        <th>PHP <c:choose><c:when test="${not empty cartsize}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${grandtotal}" /></c:when><c:otherwise>0.00</c:otherwise></c:choose></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
                <!-- /.col-md-3 -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <!-- *** FOOTER ***
 _________________________________________________________ -->
        <div id="footer" data-animate="fadeInUp">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <h4>Pages</h4>

                        <ul>
                            <li><a href="text.html">About us</a>
                            </li>
                            <li><a href="text.html">Terms and conditions</a>
                            </li>
                            <li><a href="faq.html">FAQ</a>
                            </li>
                            <li><a href="contact.html">Contact us</a>
                            </li>
                        </ul>

                        <hr>

                        <h4>User section</h4>

                        <ul>
                            <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                            </li>
                            <li><a href="register.html">Regiter</a>
                            </li>
                        </ul>

                        <hr class="hidden-md hidden-lg hidden-sm">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>Top categories</h4>

                        <h5>Men</h5>

                        <ul>
                            <li><a href="category.html">T-shirts</a>
                            </li>
                            <li><a href="category.html">Shirts</a>
                            </li>
                            <li><a href="category.html">Accessories</a>
                            </li>
                        </ul>

                        <h5>Ladies</h5>
                        <ul>
                            <li><a href="category.html">T-shirts</a>
                            </li>
                            <li><a href="category.html">Skirts</a>
                            </li>
                            <li><a href="category.html">Pants</a>
                            </li>
                            <li><a href="category.html">Accessories</a>
                            </li>
                        </ul>

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">

                        <h4>Where to find us</h4>

                        <p><strong>Obaju Ltd.</strong>
                            <br>13/25 New Avenue
                            <br>New Heaven
                            <br>45Y 73J
                            <br>England
                            <br>
                            <strong>Great Britain</strong>
                        </p>

                        <a href="contact.html">Go to contact page</a>

                        <hr class="hidden-md hidden-lg">

                    </div>
                    <!-- /.col-md-3 -->



                    <div class="col-md-3 col-sm-6">

                        <h4>Get the news</h4>

                        <p class="text-muted">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

                        <form>
                            <div class="input-group">

                                <input type="text" class="form-control">

                                <span class="input-group-btn">

			    <button class="btn btn-default" type="button">Subscribe!</button>

			</span>

                            </div>
                            <!-- /input-group -->
                        </form>

                        <hr>

                        <h4>Stay in touch</h4>

                        <p class="social">
                            <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                            <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                            <a href="#" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i></a>
                            <a href="#" class="gplus external" data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
                            <a href="#" class="email external" data-animate-hover="shake"><i class="fa fa-envelope"></i></a>
                        </p>


                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->




        <!-- *** COPYRIGHT ***
 _________________________________________________________ -->
        <div id="copyright">
            <div class="container">
                <div class="col-md-6">
                    <p class="pull-left">© 2015 Your name goes here.</p>

                </div>
                <div class="col-md-6">
                    <p class="pull-right">Template by <a href="http://bootstrapious.com/e-commerce-templates">Bootstrapious</a> with support from <a href="https://kakusei.cz">Kakusei</a> 
                        <!-- Not removing these links is part of the licence conditions of the template. Thanks for understanding :) -->
                    </p>
                </div>
            </div>
        </div>
        <!-- *** COPYRIGHT END *** -->



    </div>
    <!-- /#all -->


    

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="js/jquery-1.11.0.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/front.js"></script>



</body>

</html>
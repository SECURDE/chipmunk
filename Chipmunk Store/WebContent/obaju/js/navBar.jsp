<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="js/jquery-1.11.0.min.js"></script>  
    
    <script type="text/javascript">
            $(window).load(function(){
            	<c:if test="${sessionScope.failedLogins >= 5}">
            	alert("You have been locked down for too many failed logins, wallow in your sorrow. Come back in 20 minutes :)");
            	 // $('#login-modal').modal('show');
               </c:if>

            	
            });
        </script>
    
    
    <html>
    <div id='top'> 
        <div class='container'> 
            <div class='col-md-6 offer' data-animate='fadeInDown'> 
                    </div> 
            <div class='col-md-6' data-animate='fadeInDown'> 
                <ul class='menu'>
                
                
                <c:choose>
	                 <c:when test="${sessionScope.isLoggedIn}"> 
	              		<li style="color: white;">Hello, ${sessionScope.firstName}</li>
	                	<c:choose>
		                	<c:when test="${sessionScope.userType == 4}"> 
		                	<li><a href='LoadAdmin'>Admin Power</a></li> 
		                	</c:when>
		                	<c:when test="${sessionScope.userType == 3}"> 
			                	</c:when>
		                	<c:when test="${sessionScope.userType == 2}"> 
		                	<li><a href=' view_financial_records'>Accounting Management</a></li> 
		                	</c:when>
		                	<c:when test="${sessionScope.userType == 1}"> 
		       		                	</c:when>
	                	</c:choose>
	                	<li><a href='userData'>Your Account</a> 
	                    </li> 
	                    <li><a href='logout'>Logout</a> 
	                    </li> 
	                 </c:when>
	                 <c:otherwise>
				                    <li><a href='#' data-toggle='modal' data-target='#login-modal'>Login</a> 
				                    </li>         
			                    <li><a href='register.jsp'>Register</a> 
			                    </li>
	                 </c:otherwise>
                 </c:choose> 
                </ul> 
            </div> 
        </div> 
        <div class='modal fade' id='login-modal' tabindex='-1' role='dialog' aria-labelledby='Login' aria-hidden='true'> 
            <div class='modal-dialog modal-sm'> 
                <div class='modal-content'> 
                    <div class='modal-header'> 
                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button> 
                        <h4 class='modal-title' id='Login'>Customer login</h4> 
                    </div> 
                    <div class='modal-body'> 
                        <form  method='post' action='users'> 
                            <div class='form-group'> 
                                <input type='text' class='form-control' id='email-modal' placeholder='Username' name='email'> 
                            </div> 
                            <div class='form-group'> 
                                <input type='password' class='form-control' id='password-modal' placeholder='Password' name='password'> 
                            </div> 
                            <p class='text-center'> 
                                	 <button type='submit' class='btn btn-primary'><i class='fa fa-sign-in'></i> Log in</button>
                                	 		  </p> 
                        </form> 
                        <p class='text-center text-muted'>Not registered yet?</p> 
                        <p class='text-center text-muted'><a href='register.html'><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!</p> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
 


    <div class='navbar navbar-default yamm' role='navigation' id='navbar'> 
        <div class='container'> 
            <div class='navbar-header'> 
                <a class='navbar-brand home' href='index.jsp' data-animate-hover='bounce'> 
                    <img src='img/logo.png' alt='Obaju logo' class='hidden-xs' ALIGN=BOTTOM> 
                    <img src='img/logo-small.png' alt='Obaju logo' class='visible-xs'><span class='sr-only'>Obaju - go to homepage</span> 
                </a> 
             
                <div class='navbar-buttons'>   
      
                    <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#navigation'> 
                        <span class='sr-only'>Toggle navigation</span> 
                        <i class='fa fa-align-justify'></i> 
                    </button> 
                    
                            <a class='btn btn-default navbar-toggle' href='basket.jsp'> 
                        <i class='fa fa-shopping-cart'></i>  <span class='hidden-xs'><c:choose><c:when test="${not empty cartsize}">${cartsize}</c:when><c:otherwise>0</c:otherwise></c:choose> items in cart</span> 
                    </a>
          
                </div> 
            </div> 
            <!--/.navbar-header --> 
            <div class='navbar-collapse collapse' id='navigation'> 
            <c:choose>
            <c:when test="${sessionScope.userType == 3}"> 
            
            <ul class='nav navbar-nav navbar-left'>
                    <li class='active'><a href='index.jsp'>Home</a>
                    </li>
                     <li ><a href='productsManager?c=boots'>Boots</a>
                    </li>
                    <li ><a href='productsManager?c=shoes'>Shoes</a> 
                    </li>
                    <li ><a href='productsManager?c=sandals'>Sandals</a>
                    </li>
                    <li ><a href='productsManager?c=slippers'>Slippers</a>
                    </li>
  				</ul>          
                
  			</c:when>
  			<c:otherwise> 
  			 <ul class='nav navbar-nav navbar-left'>
                    <li class='active'><a href='index.jsp'>Home</a>
                    </li>
                     <li ><a href='products?c=boots'>Boots</a>
                    </li>
                    <li ><a href='products?c=shoes'>Shoes</a> 
                    </li>
                    <li ><a href='products?c=sandals'>Sandals</a>
                    </li>
                    <li ><a href='products?c=slippers'>Slippers</a>
                    </li>
  				</ul>
  			</c:otherwise>
  			</c:choose>
            </div>
            <!--/.nav-collapse -->
            <div class='navbar-buttons'>
                <div class='navbar-collapse collapse right' id='basket-overview'>
                    <a href='basket.jsp' class='btn btn-primary navbar-btn'><i class='fa fa-shopping-cart'></i><span class='hidden-sm'> <c:choose><c:when test="${not empty cartsize}">${cartsize}</c:when><c:otherwise>0</c:otherwise></c:choose> items in cart</span></a>
                </div>
                <!--/.nav-collapse -->
            </div>
            <div class='collapse clearfix' id='search'>
		    </span>
                    </div>
                </form>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /.container -->
    </div>

    
    
    
    </html>
    

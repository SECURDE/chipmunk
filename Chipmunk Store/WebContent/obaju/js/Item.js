/**
 * 
 */
function Item(id, category, name, price, description, rating){
	
	this.id = id;
	this.getId=function(){
		return this.id;
		};
		
	this.category = category;
	this.getCategory=function(){
		return this.category;
		};
		
	this.name= name;
	this.getName=function(){
		return this.name;
		};
		
	this.price = price;
	this.getPrice=function(){
		return this.price;
		};
		
	this.description = description;
	this.getDescription=function(){
		return this.description;
		};
		
	this.rating = rating;
	this.getRating=function(){
		return this.rating;
		};
}
